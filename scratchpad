#########################################################################################
#Build from requirements file further
#########################################################################################

# Start from a core stack version
FROM jupyter/minimal-notebook:latest

# Install other apt packages
RUN apt install graphviz

# Install from requirements.txt file
COPY --chown=${NB_UID}:${NB_GID} full-requirements.txt /tmp/
RUN pip install --requirement /tmp/full-requirements.txt --no-cache-dir && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

# Point to outpost.ws pypi mirror
COPY ./pip.conf /etc/

#########################################################################################
#Adding additional Python version into notebook
#########################################################################################
FROM jupyter/minimal-notebook:latest

# name your environment and choose python 3.x version
ARG conda_env=python37
ARG py_ver=3.7

# you can add additional libraries you want conda to install by listing them below the first line and ending with "&& \"
RUN conda create --quiet --yes -p $CONDA_DIR/envs/$conda_env python=$py_ver ipython ipykernel && \
    conda clean --all -f -y

# alternatively, you can comment out the lines above and uncomment those below
# if you'd prefer to use a YAML file present in the docker build context

# COPY --chown=${NB_UID}:${NB_GID} environment.yml /home/$NB_USER/tmp/
# RUN cd /home/$NB_USER/tmp/ && \
#     conda env create -p $CONDA_DIR/envs/$conda_env -f environment.yml && \
#     conda clean --all -f -y


# create Python 3.x environment and link it to jupyter
RUN $CONDA_DIR/envs/${conda_env}/bin/python -m ipykernel install --user --name=${conda_env} && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

# pip install from requirements file
COPY --chown=${NB_UID}:${NB_GID} full-requirements.txt /tmp/
RUN $CONDA_DIR/envs/${conda_env}/bin/pip install --requirement /tmp/full-requirements.txt --no-cache-dir && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

# prepend conda environment to path
ENV PATH $CONDA_DIR/envs/${conda_env}/bin:$PATH

# if you want this environment to be the default one, uncomment the following line:
ENV CONDA_DEFAULT_ENV ${conda_env}
COPY ./pip.conf /etc/

###############################################################################################
#Auto Sk-learn environment
###############################################################################################

ARG BASE_CONTAINER=jupyter/scipy-notebook
FROM jupyter/scipy-notebook:latest

USER root

# autosklearn requires swig, which no other image has
RUN apt-get update && \
    apt-get install -y --no-install-recommends swig && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

USER $NB_UID

RUN pip install --quiet --no-cache-dir auto-sklearn jupyterlab-git jupyterlab-gitlab

# Point to outpost.ws pypi mirror
COPY ./pip.conf /etc/

