# Start from a core stack version
FROM jupyter/minimal-notebook:python-3.8.8
USER root
RUN apt-get update && apt-get install -y gcc unixodbc-dev && apt-get install -y libboost-locale-dev graphviz curl gnupg gnupg2 gnupg1
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && curl https://packages.microsoft.com/config/ubuntu/20.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update && \
    ACCEPT_EULA=Y apt-get install -y msodbcsql17 && \
    # optional: for bcp and sqlcmd
    ACCEPT_EULA=Y apt-get install -y mssql-tools && \
    echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc && \
    source ~/.bashrc
COPY ./dremio-odbc_1.5.3.1000-2_amd64.deb /tmp
RUN dpkg -i /tmp/dremio-odbc_1.5.3.1000-2_amd64.deb && rm /tmp/dremio-odbc_1.5.3.1000-2_amd64.deb
USER $NB_UID

# Install from requirements.txt file
COPY --chown=${NB_UID}:${NB_GID} full-requirements.txt /tmp/
RUN pip install --requirement /tmp/full-requirements.txt --no-cache-dir && \
    python -m spacy download en_core_web_trf && \
    python -m spacy download en_core_web_sm && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

# Point to outpost.ws pypi mirror
COPY ./pip.conf /etc/
COPY --chown=${NB_UID}:${NB_GID} ./DenodoDialectSQLAlchemy.zip /tmp
RUN unzip /tmp/DenodoDialectSQLAlchemy.zip -d /opt/conda/lib/python3.8/site-packages/sqlalchemy/ && \
    mv /opt/conda/lib/python3.8/site-packages/sqlalchemy/denodo-sqlalchemy-dialect-8.0-20201204/denodo /opt/conda/lib/python3.8/site-packages/sqlalchemy/dialects/ && \
    rm -rf /opt/conda/lib/python3.8/site-packages/sqlalchemy/denodo-sqlalchemy-dialect-8.0-20201204 && \
    chattr -i -a /tmp/DenodoDialectSQLAlchemy.zip && \
    chmod ugo+w /tmp/DenodoDialectSQLAlchemy.zip && \
    rm /tmp/DenodoDialectSQLAlchemy.zip    

USER root
RUN apt-get update && apt-get install -y tesseract-ocr poppler-utils && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
USER $NB_UID

